# Get Random Transport Service for sending email.
Function Get-RandomTransportService {
	$AdSite = nltest /server:$env:logonserver /dsgetsite | select -first 1
	Get-TransportService | foreach {
		param([string]$TransportServer = $_.name)
		If ((nltest /server:$TransportServer /dsgetsite | select -first 1) -eq $AdSite){
			If (Test-ServiceHealth -server $TransportServer | where {$_.ServicesRunning -like "MSExchangeTransport"}) {
				$TransportServers += @($TransportServer)
			}
		}
	}
	$TransportServers | Get-Random
}